const fabController = require('../fabric/fabric-controller');
module.exports = {


  friendlyName: 'Join bu',


  description: '',


  inputs: {
    buId: {
      description: 'BU id',
      type: 'string',
      required: true
    }

  },


  exits: {
    notFound: {
      responseType: 'notFound'
    }


  },


  fn: async function (inputs, exits) {

    sails.log.info(`"Join BU" Request: ${JSON.stringify(inputs)}`);

    const buId = inputs.buId;
    const cId = this.req.me.id;

    if (!buId) {
      throw 'notFound';
    }

    try {
      await fabController.joinBU(cId, buId);

      return exits.success();
    } catch (err) {
      var error = {};
      error.error = err.message;
      return error;
    }

  }
};
