const fabController = require('../fabric/fabric-controller');
const moment = require('moment');

module.exports = {


  friendlyName: 'View summary',


  description: 'Display "Summary" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/dashboard/summary'
    }

  },


  fn: async function (inputs, exits) {

    var allBuMap = new Map();
    if (this.req.domainData.AllBU) {
      for (i = 0; i < this.req.domainData.AllBU.length; i++) {
        allBuMap.set(this.req.domainData.AllBU[i].id, this.req.domainData.AllBU[i]);
      }
    }

    var tradeRecords = [];
    if (allBuMap.size > 0) {
      const trades = await fabController.findTradeByCustomerId(this.req.me.id);
      if (!trades.error ) {
        for (const temp of trades) {
          if (!allBuMap.has(temp.bu.getIdentifier())) {
            continue;
          }
          const bu = allBuMap.get(temp.bu.getIdentifier());
          const buId = temp.bu.getIdentifier();
          const buName = bu.name;
          // if (!tradeRecords.has(temp.bu.buId)) {
          //   tradeRecords.set(temp.bu.buId, new Array());
          // }
          let usedTime = '';
          let valid = true;
          if (temp.usedTime) {
            usedTime = moment(temp.usedTime).format('DD/MMM/YYYY HH:mm');
            valid = false;
          }
          const point = temp.point;
          const ptPerCoinRatio = bu.ptToCoinRatio;
          let expectedCoin = Math.round(point / ptPerCoinRatio * 100) / 100;

          const trade = {
            id: temp.tradeId,
            point: point,
            date: moment(temp.clientSubmitTime).format('DD/MMM/YYYY HH:mm'),
            buId: buId,
            buName: buName,
            valid: valid, //temp.status == "Active" ? true : false,
            usedTime: usedTime,
            ptPerCoinRatio: ptPerCoinRatio,
            expectedCoin: expectedCoin
          };
          tradeRecords.push(trade);
        }
      }
      tradeRecords.sort(function (a, b) {
        if (a.buId < b.buId) {
          return -1;
        } else if (a.buId > b.buId) {
          return 1;
        }
        return 0;
      });
    }

    sails.log.info(`All Trades: ${JSON.stringify(tradeRecords)}`);

    // Respond with view.
    return exits.success({
      allBu: this.req.domainData.AllBU,
      tradeRecords: tradeRecords
    });

  }


};
