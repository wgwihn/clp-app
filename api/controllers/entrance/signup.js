const fabController = require('../fabric/fabric-controller');
module.exports = {


  friendlyName: 'Signup',


  description: 'Sign up for a new user account.',


  extendedDescription: '',


  inputs: {

    emailAddress: {
      required: true,
      type: 'string',
      isEmail: true,
      description: 'The email address for the new account, e.g. m@example.com.',
      extendedDescription: 'Must be a valid email address.',
    },

    password: {
      required: true,
      type: 'string',
      maxLength: 200,
      example: 'password',
      description: 'The unencrypted password to use for the new account.'
    },

    fullName:  {
      required: true,
      type: 'string',
      example: 'Frida Kahlo de Rivera',
      description: 'The user\'s full name.',
    }

  },


  exits: {

    invalid: {
      responseType: 'badRequest',
      description: 'The provided fullName, password and/or email address are invalid.',
      extendedDescription: 'If this request was sent from a graphical user interface, the request '+
      'parameters should have been validated/coerced _before_ they were sent.'
    },

    emailAlreadyInUse: {
      statusCode: 409,
      description: 'The provided email address is already in use.',
    },

  },


  fn: async function (inputs, exits) {

    var name = inputs.fullName;
    var newEmailAddress = inputs.emailAddress.toLowerCase();
    var password = inputs.password;
    var tosAcceptedByIp = this.req.ip;

    sails.log.info("Sign up request for Name: " + name + ", email: " + newEmailAddress);

    var result = await fabController.registerCustomer(name, newEmailAddress, password)

    if (result.error) {
      return exits.emailAlreadyInUse();
    } else {
      this.req.session.userId = result.id;
      sails.log.info("Sign up successful for ID: " + result.id + ", name: " + result.name);
    }

    // Since everything went ok, send our 200 response.
    return exits.success();

  }

};
