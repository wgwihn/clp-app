//export module
module.exports = {

    'NAMESPACE': 'org.example.clpnet',

    'NETWORK_NAME': 'clp-net',
    // these are the credentials to use to connect to the Hyperledger Fabric
    'CARD_ID': 'admin@clp-net',

    'MODEL': {
      'Customer': {
        'NAME': 'Customer',
        'REGISTRY': 'org.example.clpnet.Customer',

      }
    },

    getModelName: function () {

    },

    getModelRegistry: function() {

    },

    registry: function (reg) {
      return NAMESPACE + '.' + reg;
    }
}
