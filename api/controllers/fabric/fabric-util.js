//export module

const NAMESPACE = 'org.example.clpnet';

const NETWORK_NAME = 'clp-net';
// these are the credentials to use to connect to the Hyperledger Fabric
const CARD_ID = 'admin@clp-net';

module.exports = {

  // 'MODEL': {
  //   'Customer': {
  //     'NAME': 'Customer',
  //     'REGISTRY': 'org.example.clpnet.Customer',
  //
  //   }
  // },


  getRegistry: function (reg) {
    return NAMESPACE + '.' + reg;
  },

  toFabricModelId: function (reg, id) {
    return 'resource:' + NAMESPACE + '.' + reg + '#' + id;
  },

  fromFabricModelId: function (reg, modelId, isRelationship) {
    if (isRelationship) {

    }
    // 'resource:org.example.clpnet.' + reg + '#' + id
    // 29 + reg.length()
    return modelId.substr(29 + reg.length);
  },

  getBuNameByBuId: function (buId) {
    console.log(JSON.stringify(this.req));
    if (this.req.domainData && this.req.domainData.AllBU) {
      for (const bu of this.req.domainData.AllBU) {
        if (buId == bu.id) {
          console.log(bu.name);
          return bu.name;
        }
      }
    }
    return null;
  }


}
