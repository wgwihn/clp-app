const fabController = require('../fabric/fabric-controller');
module.exports = {


  friendlyName: 'Exchange reward',


  description: ' ',


  inputs: {
    rpId: {
      description: 'Reward Post id',
      type: 'string',
      required: true
    }
  },


  exits: {
    notFound: {
      responseType: 'notFound'
    }
  },


  fn: async function (inputs, exits) {
    sails.log.info(`"Exchange reward" Request: ${JSON.stringify(inputs)}`);

    const rpId = inputs.rpId;
    const cId = this.req.me.id;

    if (!rpId) {
      throw 'notFound';
    }

    try {
      const reward = await fabController.exchangeRewardPost(cId, rpId);

      if (reward.id) {
        return exits.success();
      }

      if (reward.error) {
        throw 'Fail to exchange reward.';
      }

    } catch (err) {
      var error = {};
      error.error = err.message;
      return error;
    }

  }


};
