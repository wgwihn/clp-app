const fabController = require('../fabric/fabric-controller');
module.exports = {


  friendlyName: 'Add trade',


  description: '',


  inputs: {
    buId: {
      description: 'BU id',
      type: 'string',
      required: true
    },

    point: {
      description: 'Point of the trade',
      type: 'number',
      required: true
    }
  },


  exits: {
    notFound: {
      responseType: 'notFound'
    },

    success: {
      // viewTemplatePath: 'pages/dashboard/summary'
    }
  },


  fn: async function (inputs, exits) {

    sails.log.info(`"Add Trade" Request: ${JSON.stringify(inputs)}`);

    const cId = this.req.me.id;
    const buId = inputs.buId;
    const point = inputs.point;

    if (!cId) {
      throw 'notFound';
    }

    try {
      let trade = await fabController.addTrade(cId, buId, point, new Date());

      sails.log.info(`"Add Trade" trade: ${JSON.stringify(trade)}`);

      if (trade.error) {
        throw 'Fail to create a trade.';
      }

      return exits.success({
        trade: trade
      });
    } catch (err) {
      var error = {};
      error.error = err.message;
      return error;
    }

  }
};
