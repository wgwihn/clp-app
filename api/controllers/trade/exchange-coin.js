const fabController = require('../fabric/fabric-controller');
module.exports = {


  friendlyName: 'Exchange coin',


  description: '',


  inputs: {
    tId: {
      description: 'Trade id',
      type: 'string',
      required: true
    }
  },


  exits: {
    notFound: {
      responseType: 'notFound'
    }

  },


  fn: async function (inputs, exits) {
    sails.log.info(`"Exchange coin" Request: ${JSON.stringify(inputs)}`);

    const tId = inputs.tId;
    const cId = this.req.me.id;

    if (!tId) {
      throw 'notFound';
    }

    try {
      const trade = await fabController.exchangeCoin(cId, tId);

      if (trade.id) {
        return exits.success({
          trade: trade
        });
      }

      if (trade.error) {
        throw 'Fail to exchange coin.';
      }

    } catch (err) {
      var error = {};
      error.error = err.message;
      return error;
    }

  }


};
