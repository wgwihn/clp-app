const fabController = require('./fabric/fabric-controller');
module.exports = {


  friendlyName: 'View reward history',


  description: 'Display "Reward history" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/reward-history'
    }

  },


  fn: async function (inputs, exits) {

    let historyArray = await fabController.findRewardByCustomerId(this.req.me.id);

    if (historyArray.error) {
      console.log(historyArray.error);
      historyArray = [];
    }

    // Respond with view.
    return exits.success({
      historyArray: historyArray
    });

  }


};
