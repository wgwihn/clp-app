const fabController = require('./fabric/fabric-controller');
const moment = require('moment');
module.exports = {


  friendlyName: 'View reward',


  description: 'Display "Reward" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/reward',
      description: 'For reward'
    }

  },


  fn: async function (inputs, exits) {

    let rewardsPosts = await fabController.findallRewardPost(this.req.me.id);

    sails.log.info(`All rewards posts: ${JSON.stringify(rewardsPosts)}`);

    if (rewardsPosts.error) {
      rewardsPosts = [];
    }

    return exits.success({
      rewardsPosts: rewardsPosts

    });

  }


};
