
parasails.registerPage('summary', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {

    response: {},

    tradeRecords: [],
    joinedBU: [],
    // For add Trade Start
    allBU: [],
    addTradeData: {
      buId: undefined,
      point: undefined
    },
    addTradeErrors: {},
    addTradeModalOpen: false,
    // For add Trade End

    // For join BU start
    joinBUModalOpen: false,
    selectedBU: {},
    joinBUErrors: {},
    // For join BU end

    // For exchange coin start
    exchangeCoinModalOpen: false,
    selectedTrade: {},
    exchangeCoinErrors: {},
    // For exchange coin end


    // Syncing / loading state
    syncing: false,

    // Server error state
    cloudError: ''
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);

    var allBuMap = new Map();
    if (this.domainData.AllBU) {
      for (i = 0; i < this.domainData.AllBU.length; i++) {
        allBuMap.set(this.domainData.AllBU[i].id, this.domainData.AllBU[i]);
      }
    }

    this.allBU = [];
    this.joinedBU = [];
    // this.buOptions.push({
    //   id: "XXX", name: "Please choose a company"
    // });
    if (allBuMap) {
      for (var bu of allBuMap.values()) {
          if (bu.isJoined) {
            this.joinedBU.push(bu);
          }
          this.allBU.push(bu);
      }
    }


  },

  mounted: async function() {

  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    clickAddButton: function() {
      if (this.joinedBU.length > 0) {
        // Open the modal.
        this.addTradeModalOpen = true;
      } else {
        alert('Please Join a company.');
      }

    },

    closeAddTradeModal: function () {
      this.addTradeModalOpen = false;
      this.addTradeErrors = {};
      this.addTradeData = {};
      cloudError: '';
    },

    handleParsingAddTradeForm: function () {
      // Clear out any pre-existing error messages.
      this.addTradeErrors = {};

      var argins = this.addTradeData;

      console.log('AddTradeForm Data: ' + JSON.stringify(argins));

      if (!argins.buId || !argins.buId.startsWith('Bx')) {
        this.addTradeErrors.buId = true;
      }

      if (!argins.point || argins.point < 1) {
        this.addTradeErrors.point = true;
      }

      if (Object.keys(this.addTradeErrors).length > 0) {
        console.log("handleParsingAddTradeForm error: " + JSON.stringify(this.addTradeErrors));
        return;
      }

      return argins;
    },

    submittedAddTradeForm: function () {
      console.log("submittedAddTradeForm");
      this.addTradeModalOpen = false;
      location.reload();
    },
    ////////////////////////////////////////////////////////////////
    clickJoinButton: function(buId) {
      if (buId) {
        for (i = 0; i < this.allBU.length; i++) {
          if (this.allBU[i].id == buId && this.allBU[i].isJoined == false) {
            this.selectedBU = this.allBU[i];
          }
        }
      }
      if (this.selectedBU && this.selectedBU.id) {
        console.log("selectedBU: " + JSON.stringify(this.selectedBU));
        // Open the modal.
        this.joinBUModalOpen = true;
      }
    },

    closeJoinBUModal: function () {
      this.joinBUModalOpen = false;
      this.joinBUErrors = {};
      this.selectedBU = {};
      cloudError: '';
    },

    handleParsingJoinBUForm: function () {
      this.joinBUErrors = {};

      var argins = this.selectedBU;

      console.log('JoinBUForm Data: ' + JSON.stringify(argins));

      if (!argins.id || !argins.id.startsWith('Bx')) {
        this.joinBUErrors.buId = true;
      }

      if (Object.keys(this.joinBUErrors).length > 0) {
        console.log("handleParsingJoinBUForm error: " + JSON.stringify(this.joinBUErrors));
        return;
      }

      return {
        buId: argins.id
      };
    },

    submittedJoinBUForm: function () {
      console.log("submittedJoinBUForm");
      this.joinBUModalOpen = false;
      location.reload();
    },
    ////////////////////////////////////////////////////////////////
    clickUsedButton:function (tId) {
      if (tId) {
        for (i = 0; i < this.tradeRecords.length; i++) {
          if (this.tradeRecords[i].id == tId && this.tradeRecords[i].valid == true) {
            this.selectedTrade = this.tradeRecords[i];
          }
        }
      }
      if (this.selectedTrade && this.selectedTrade.id) {
        console.log("selectedTrade: " + JSON.stringify(this.selectedTrade));
        // Open the modal.
        this.exchangeCoinModalOpen = true;
      }
    },

    closeExchangeCoinModal: function () {
      this.exchangeCoinModalOpen = false;
      this.exchangeCoinErrors = {};
      this.selectedTrade = {};
      cloudError: '';
    },

    handleParsingExchangeCoinForm: function () {
      this.exchangeCoinErrors = {};

      var argins = this.selectedTrade;

      console.log('Exchange Coin Form Data: ' + JSON.stringify(argins));

      if (!argins.id || !argins.id.startsWith('Tx')) {
        this.exchangeCoinErrors.tId = true;
      }
      if (!argins.valid) {
        this.exchangeCoinErrors.invalid = true;
      }

      if (Object.keys(this.exchangeCoinErrors).length > 0) {
        console.log("handleParsingExchangeCoinForm error: " + JSON.stringify(this.exchangeCoinErrors));
        return;
      }


      return {
        tId: argins.id
      };
    },

    submittedExchangeCoinForm: function () {
      console.log("submittedExchangeCoinForm");
      this.exchangeCoinModalOpen = false;
      location.reload();
    },
  }
});
