parasails.registerPage('reward-history', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    historyArray: [],

    // For view detail reward info start
    detailModalOpen: false,
    selectedReward: {}
    // For view detail reward info end
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
  },
  mounted: async function() {
    //…
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    clickDetailButton: function (rewardId) {
      if (rewardId) {
        for (i = 0; i < this.historyArray.length; i++) {
          if (this.historyArray[i].id == rewardId) {
            this.selectedReward = this.historyArray[i];
          }
        }
      }
      if (this.selectedReward && this.selectedReward.id) {
        console.log("selectedReward: " + JSON.stringify(this.selectedReward));
        // Open the modal.
        this.detailModalOpen = true;
      }
    },

    closeDetailModal: function () {
      this.detailModalOpen = false;
      this.detailErrors = {};
      this.selectedReward = {};
      cloudError: '';
    }
  }
});
