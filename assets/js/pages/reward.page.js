parasails.registerPage('reward', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    rewardsPosts: [],


    // For exchange reward post start
    exchangeRewardPostModalOpen: false,
    selectedRewardPost: {},
    exchangeRewardPostErrors: {},
    // For exchange reward post end

    // Syncing / loading state
    syncing: false,

    // Server error state
    cloudError: '',
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
  },
  mounted: async function() {
    //…
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {
    clickExchangeButton: function (rpId) {
      if (rpId) {
        for (i = 0; i < this.rewardsPosts.length; i++) {
          if (this.rewardsPosts[i].id == rpId) {
            this.selectedRewardPost = this.rewardsPosts[i];
          }
        }
        if (this.selectedRewardPost) {
          console.log("selectedRewardPost: " + JSON.stringify(this.selectedRewardPost));
          this.exchangeRewardPostModalOpen = true;

        }
      }
    },

    closeExchangeRewardPostModal: function () {
      this.exchangeRewardPostModalOpen = false;
      this.exchangeRewardPostErrors = {};
      this.selectedRewardPost = {};
      cloudError: '';
    },

    handleParsingExchangeRewardPostForm: function () {
      this.exchangeRewardPostErrors = {};

      var argins = this.selectedRewardPost;

      console.log('Exchange Reward Post Form Data: ' + JSON.stringify(argins));

      if (!argins.allowRedeem) {
        this.exchangeRewardPostErrors.notAllowRedeem = true;
      }

      const coin = this.me.coin;
      if (argins.coin > coin) {
        this.exchangeRewardPostErrors.notEnoughCoin = true;
      }

      if (Object.keys(this.exchangeRewardPostErrors).length > 0) {
        console.log("handleParsingExchangeRewardPostForm error: " + JSON.stringify(this.exchangeRewardPostErrors));
        return;
      }

      return {
        rpId: argins.id
      };
    },

    submittedExchangeRewardPostForm: function () {
      console.log("submittedExchangeRewardPostForm");
      this.exchangeRewardPostModalOpen = false;
      location.reload();
    },
  }
});
