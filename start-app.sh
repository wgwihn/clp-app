. ~/.nvm/nvm.sh
. ~/.profile
. ~/.bashrc
nvm use 8.12.0
rm app.log
nohup sails lift > app.log 2>&1 &
echo $! > pid
( tail -f -n0 app.log & ) | grep -q "info: Server lifted in"
cat app.log
