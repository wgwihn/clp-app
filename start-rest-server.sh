. ~/.nvm/nvm.sh
. ~/.profile
. ~/.bashrc
nvm use 8.12.0
cd ~/fabric-tools/
rm rest-server.log
nohup composer-rest-server -c admin@clp-net -n never -u true -d n > rest-server.log 2>&1 &
echo $! > rest-pid
( tail -f -n0 rest-server.log & ) | grep -q "Browse your REST API"
cat rest-server.log
