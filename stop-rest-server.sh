nvm use 8.12.0
cd ~/fabric-tools/
kill -9 `cat rest-pid`
rm rest-pid
echo "Rest server stopped."
